<?php
/**
 * @file
 * Plugin to add JS / CSS files.
 */

/**
 * Plugin to add JS / CSS files.
 *
 * Uses "render first" which seems not to be documented but supported by the
 * standard panels render class.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Library Loader'),
  'description' => t('Ensures the required libraries are loaded. Has no effect on caching keys.'),
  'category' => t('Caching helper'),
  'render first' => TRUE,
  'defaults' => array(
    'system_libraries' => array(),
    'libraries_libraries' => array(),
    'custom_files' => array(),
  ),
  'render callback' => 'library_loader_ctools_library_loader_content_type_render',
  'edit form' => 'library_loader_ctools_library_loader_content_type_edit_form',
);

/**
 * Create a context, from manual configuration or from an argument on the URL.
 *
 * @param string $subtype
 *   The requested subtype
 * @param array $conf
 *   The content type configuration
 * @param array $panel_args
 *   The arguments provided to the owner of the content type. Some content may
 *   wish to configure itself based on the arguments the panel or dashboard
 *   received.
 * @param array $pane_context
 *   Panel context.
 * @param string $incoming_content
 *   Any incoming content, if this display is a wrapper.
 */
function library_loader_ctools_library_loader_content_type_render($subtype, $conf, $panel_args, $pane_context, $incoming_content) {
  if (!empty($conf['system_libraries'])) {
    foreach ($conf['system_libraries'] as $module => $libraries) {
      foreach ($libraries as $library => $val) {
        drupal_add_library($module, $library);
      }
    }
  }
  if (module_exists('libraries') && !empty($conf['libraries_libraries'])) {
    foreach ($conf['libraries_libraries'] as $library => $value) {
      libraries_load($library);
    }
  }
  if (!empty($conf['custom_files'])) {
    foreach ($conf['custom_files'] as $file) {
      switch ($file['type']) {
        case 'js':
          drupal_add_js($file['path'], array('scope' => $file['scope']));
          break;

        case 'css':
          drupal_add_css($file['path'], array('scope' => $file['scope']));
          break;
      }
    }
  }
}

/**
 * Settings form.
 */
function library_loader_ctools_library_loader_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'] + $form_state['plugin']['defaults'];

  $form['override_title']['#access'] = FALSE;
  $form['override_title_text']['#access'] = FALSE;
  $form['override_title_markup']['#access'] = FALSE;

  // System libraries.
  // @see drupal_get_library()
  $sytem_libraries = array();
  foreach (module_implements('library') as $module) {
    $module_libraries = module_invoke($module, 'library');
    if (empty($module_libraries)) {
      $module_libraries = array();
    }
    // Allow modules to alter the module's registered libraries.
    drupal_alter('library', $module_libraries, $module);
    foreach ($module_libraries as $key => $data) {
      if (is_array($data)) {
        // Add default elements to allow for easier processing.
        $module_libraries[$key] += array(
          'dependencies' => array(),
          'js' => array(),
          'css' => array(),
        );
        foreach ($module_libraries[$key]['js'] as $file => $options) {
          $module_libraries[$key]['js'][$file]['version'] = $module_libraries[$key]['version'];
        }
      }
    }
    $sytem_libraries[$module] = $module_libraries;
  }
  $form['system_libraries'] = array(
    '#title' => t('System libraries'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );
  $selected = 0;
  foreach ($sytem_libraries as $module => $libraries) {
    foreach ($libraries as $library => $settings) {
      $selected += (int) !empty($conf['system_libraries'][$module][$library]);
      $form['system_libraries'][$module][$library] = array(
        '#title' => $module . ' ' . $library,
        '#type' => 'checkbox',
        '#default_value' => !empty($conf['system_libraries'][$module][$library]),
      );
    }
  }
  $form['system_libraries']['#title'] .= ' [' . $selected . ']';

  // Libraries libraries.
  if (module_exists('libraries')) {
    $form['libraries_libraries'] = array(
      '#title' => t('Libraries libraries') . ' [' . count(array_filter($conf['libraries_libraries'])) . ']',
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
    );
    foreach (libraries_info() as $library => $settings) {
      $form['libraries_libraries'][$library] = array(
        '#title' => $settings['name'],
        '#type' => 'checkbox',
        '#default_value' => !empty($conf['libraries_libraries'][$library]),
      );
    }
  }

  // Custom files.
  $form['custom_files'] = array(
    '#title' => t('Define custom files here'),
    '#description' => t('Hint: use hook_library() to define the dependencies and not this feature ;)'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => count(array_filter($conf['custom_files'])) == 0,
    '#tree' => TRUE,
  );
  foreach ($conf['custom_files'] as $i => $file_settings) {
    $form['custom_files'][$i] = library_loader_ctools_library_loader_settings_form_file_element($file_settings);
  }
  $form['custom_files'][++$i] = library_loader_ctools_library_loader_settings_form_file_element(array());

  return $form;
}

/**
 * Cleanup the values and ensure no evil paths are injected.
 */
function library_loader_ctools_library_loader_content_type_edit_form_validate($form, &$form_state) {
  foreach ($form_state['values']['custom_files'] as $i => $file_settings) {
    if (!empty($file_settings['path'])) {
      $path = $file_settings['path'];
      if (!is_file(DRUPAL_ROOT . '/' . $path) || strpos($path, '../') !== FALSE || strpos($path, '..\\') !== FALSE) {
        form_set_error('custom_files][' . $i, 'Invalid file specified, file not found.');
      }
    }
    else {
      unset($form_state['values']['custom_files'][$i]);
    }
  }
}

/**
 * Settings form.
 */
function library_loader_ctools_library_loader_content_type_edit_form_submit($form, &$form_state) {
  // Cleanup values to store.
  foreach ($form_state['values']['system_libraries'] as $module => $libraries) {
    $form_state['values']['system_libraries'][$module] = array_filter($libraries);
  }
  $form_state['values']['system_libraries'] = array_filter($form_state['values']['system_libraries']);
  $form_state['values']['libraries_libraries'] = array_filter($form_state['values']['libraries_libraries']);

  foreach (array('system_libraries', 'libraries_libraries', 'custom_files') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Returns a fieldset to configure a custom file.
 *
 * @param array $file_settings
 *   The file settings to use as defaults.
 *
 * @return array
 *   The form element to configure a custom file.
 */
function library_loader_ctools_library_loader_settings_form_file_element($file_settings) {
  static $count;
  if (!isset($count)) {
    $count = 0;
  }
  $count++;

  $file_settings += array(
    'path' => '',
    'type' => 'js',
    'scope' => 'header',
  );

  $element = array(
    '#title' => '#' . $count,
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#tree' => TRUE,
    '#attributes' => array(
      'class' => array('container-inline'),
    ),
  );
  $element['path'] = array(
    '#title' => t('Path to file'),
    '#type' => 'textfield',
    '#default_value' => $file_settings['path'],
  );
  $element['type'] = array(
    '#type' => 'select',
    '#options' => array(
      'js' => 'Javascript',
      'css' => 'Stylesheet',
    ),
    '#default_value' => $file_settings['type'],
  );
  $element['scope'] = array(
    '#type' => 'select',
    '#options' => array(
      'header' => t('Header'),
      'footer' => t('Footer'),
    ),
    '#default_value' => $file_settings['scope'],
  );
  return $element;
}
